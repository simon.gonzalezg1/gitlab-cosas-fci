import numpy as np
class pendbalis():
    def __init__(self, m, M, v, R, g):
        self.massbull = m
        self.massblock = M
        self.vel = v
        self.radius = R
        self.gravity = g

    def maxangle(self):
        if self.vel >= np.sqrt(4 * self.gravity * 
                                    self.radius* (self.massbull 
                                    + self.massblock)/self.massbull):
            return np.pi
        theta = np.arccos(1 - self.massbull/(self.massblock + self.massbull) * self.vel**2 / (2*self.radius*self.gravity))
        return theta

