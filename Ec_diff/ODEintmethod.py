import numpy as np
class numericmethod():
    def __init__(self, f, x0, y0, n):
        self.f = f
        self.x0 = x0
        self.y0 = y0
        self.n = n

    def h(self, xf):
        if xf <= self.x0:
            raise Exception("Ingrese un valor final mayor al inicial")
        return (xf - self.x0)/self.n
    
    def arrX(self, xf):
        if xf <= self.x0:
            raise Exception("Ingrese un valor final mayor al inicial")
        h = self.h(xf)
        x = np.linspace(self.x0, xf + h, self.n)
        return x


    def inteuler(self, xf):
        h = self.h(xf)
        x = self.arrX(xf)
        y = np.array([self.y0])
        for i in x[:-1]:
            ytemp = y[-1] + h * self.f(i,y[-1])
            y = np.append(y, ytemp)
        return y
    
    def intrungekutta4(self,xf):
        h = self.h(xf)
        x = self.arrX(xf)
        y = np.array([self.y0])
        for i in x[:-1]:
            k1 = self.f(i,y[-1])
            k2 = self.f(i + h/2, y[-1] + h*k1/2)
            k3 = self.f(i + h/2, y[-1] + h*k2/2)
            k4 = self.f(i + h, y[-1] + h*k3)
            ytemp = y[-1] + h/6 * (k1 + 2*k2 + 2*k3 + k4)
            y = np.append(y, ytemp)
        return y
    
    def evaleuler(self, x):
        y = self.inteuler(x)[-1]
        return y
    
    def evalrungekutta4(self, x):
        y = self.rungekutta4(x)[-1]
        return y
