from sympy import Function, dsolve, Eq, Derivative, sin, cos, symbols, lambdify
from sympy.abc import x
class exactsol():
    def __init__(self, f, x0, y0, n):
        self.f = f
        self.x0 = x0
        self.y0 = y0
        self.n = n

    def exactsol(self):
        y = Function('y')
        ics = {y(self.x0): self.y0}
        sol = dsolve(self.f, y(x), ics=ics)
        funcsol = lambdify(sol)