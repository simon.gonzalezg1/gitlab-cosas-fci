from movparabolico import *
import numpy as np
import matplotlib.pyplot as plt
class movparabolicoacelx(movparabolico):
    # Se hereda de la clase para el movimiento parabolico:
    def __init__(self, x0=0., y0=0., v0=0., alpha=0.):
        super().__init__(x0, y0, v0, alpha)
    
    # Posicion en x dado un tiempo y una aceleracion en x:
    def xac(self, t, ax):
        return self.x0 + self.v0x() * t + ax * t**2 * 1/2
    
    # Posiciones en x durante la trayectoria dadas aceleraciones en x y en y:
    def arrxac(self, ax, ay):
        t = self.arrtime(ay)
        x = np.array([])
        for i in t:
            x = np.append(x, self.xac(i, ax))
        return x

    # Metodo que grafica la trayectoria ahora con aceleracion en x y en y:
    def graphxac(self, ax, ay):
        
        tfin = self.flightime(ay)
        x = self.arrxac(ax, ay)
        y = self.arry(ay)
        
        plt.figure(figsize=(10,8))
        plt.plot(x, y , label="Trayectoria",color="black")
        plt.scatter(self.x0, self.y0, label="Punto inicial",color="blue")
        plt.scatter(self.xac(tfin, ax), self.y(tfin,ay), label="Punto final",color="red")
        plt.grid()
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.legend()
        plt.savefig("figura_xac.png")