class humano():
    # Constructor
    def __init__(self, name= "NN", age= 0):
        print("name_class: {}".format(__name__))
        self.name = name
        self.age = age
    
    # Método 
    def viewAge(self):
        a = self.age
        return a

    def viewName(self):
        return self.name

    def info_humano(self):
        return "{} tiene {}".format(self.viewName(), self.viewAge())