import numpy as np
import matplotlib.pyplot as plt
# Tamaño de matriz
L = 2

# Matriz de Spines
S = np.ones((L,L)) - 2 *np.random.randint(0,2,(L,L))

# Mascara para la suma de aledaños
Smask = np.zeros((L+2,L+2))
Smask[np.ix_(range(1,L+1),range(1,L+1))] = S

# Calculando suma de aledaños:
h = 0
for i in range(1,L):
    for j in range(1,L):
     h -= Smask[i+1,j] - Smask[i-1,j] - Smask[i,j+1] - Smask[i,j-1]            
h = h/2

# Calculando corte
corte = np.random.uniform(0,1,(L,L))

# Temperaturas
T = np.arange(0.01,100)
B = 1.0/T
pi = np.array([])
for i in B:
    Snew = -np.ones((L,L))
    pi = np.append(pi,(1+np.exp(-i*2*h))**(-1))
    pi <= corte
    Snew == S

    
print(pi)