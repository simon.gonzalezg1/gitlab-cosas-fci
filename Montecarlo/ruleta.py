import numpy as np
import matplotlib.pyplot as plt
n = 10000
death = 5
nrulet = 9

x = np.random.randint(1,nrulet+1,n)
cond = (x == 5)
prob = cond.sum()/n

deaths = np.array([])
for i in range(1,n):
    n = i
    x = np.random.randint(1,nrulet+1,n)
    cond = (x == 5)
    probtemp = cond.sum()/n
    deaths = np.append(deaths,probtemp)

plt.figure()
plt.plot(range(1,n+1), deaths)
plt.show()

