import numpy as np
from numpy.random import uniform
import matplotlib.pyplot as plt
import random
if __name__ == "__main__":
    
    def pi(r, n):
        x,y = uniform(-r, r, size=(2,n))
        interior  = (x**2 + y**2)<=r**2
        return 4*np.sum(interior)/n 

    random.seed(10)
    print(pi(1,10000))
    arrpi = np.array([])
    for j in range(1,10000):
        arrpi = np.append(arrpi, pi(1,j))


    plt.figure()
    plt.scatter(range(1,10000), arrpi)
    plt.show()

    def graphpi(r, n):
        x,y = uniform(-r, r, size=(2,n))
        interior  = (x**2 + y**2)<=r**2
        exterior = np.invert(interior)
        plt.figure(figsize=(10,10))
        plt.scatter(x[interior], y[interior], color="red")
        plt.scatter(x[exterior], y[exterior], color="blue")
        plt.show()

    graphpi(1,1000)
