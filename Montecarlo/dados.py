import numpy as np
import matplotlib.pyplot as plt
n = range(1,10000)
prop = np.array([])
for i in n:
    n1,n2 = np.random.randint(1, 7,(2,i))
    ntot = n1 + n2
    is7 = (ntot == 7)
    proptemp = is7.sum()/(i)
    prop = np.append(prop, proptemp)

plt.figure()
plt.scatter(n, prop)
plt.show()
    