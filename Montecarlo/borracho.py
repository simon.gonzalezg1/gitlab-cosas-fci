import numpy as np
import matplotlib.pyplot as plt
n = 10000
dx, dy = np.random.randint(-1,2,(2, n))
x = np.array([])
y = np.array([])
for i in range(n):
    x = np.append(x,np.sum(dx[:i]))
    y = np.append(y,np.sum(dy[:i]))
plt.figure()
plt.plot(x,y,"r")
plt.show()
print(dx[:3])