import numpy as np
import matplotlib.pyplot as plt
NRa = 1000
NAc = 0
lambdaRa = np.log(2)/14.8 # En días
lambdaAc = np.log(2)/10.8 # En días
dt = 0.01
t = np.arange(0,100,dt)
NRat = NRa * np.exp(-lambdaRa*t)
NAct = lambdaRa/(lambdaAc - lambdaRa) * NRa * (np.exp(-lambdaRa*t) - np.exp(-lambdaAc*t) )
#plt.figure()
#plt.plot(t,NRat)
#plt.plot(t,NAct)
#plt.show()

PRa = 1 - np.exp(-lambdaRa*dt)
PAc = 1 - np.exp(-lambdaAc*dt)



NRalist = np.array([1000])
NAclist = np.array([0])


for i in t:
    RandRa = np.random.uniform(0,1, NRalist[-1])
    RandAc = np.random.uniform(0,1, NAclist[-1])
    
    decayRa = RandRa < PRa*np.ones(len(RandRa))
    decayAc = RandAc < PAc*np.ones(len(RandAc))

    NRalist = np.append(NRalist, NRalist[-1]-np.sum(decayRa))
    NAclist = np.append(NAclist, NAclist[-1]+np.sum(decayRa)-np.sum(decayAc))

plt.figure()
plt.plot(t, NRalist[:-1])
plt.plot(t, NAclist[:-1])
plt.title("Montecarlo")
plt.show()
