import numpy as np
from sympy import symbols, integrate
import matplotlib.pyplot as plt 
# Parametros
a = 0
b = 1
n = 10000
f = lambda x: 1-x**2

# Por promedio de areas

arrx = np.random.uniform(a,b,n)
arry = f(arrx)
area = (b-a)*np.sum(arry)/n
#print(area)

z = symbols("z")
#print(integrate(f(z), (z,a,b)))

t = np.arange(a,b,0.01)
y = f(t)
plt.figure()
plt.plot(t, y)
#plt.show()

# Por puntos al azar

x = np.random.uniform(a,b,n)
yrand = np.random.uniform(0,np.max(y),n)
yreal = f(x)
puntden = np.sum(np.abs(yrand) <= np.abs(yreal))
area1 = np.max(y)*(b-a)*puntden/n
#print(area1)

# Con diferencia de area

g = lambda x: x**2
h = lambda x: x**(1/2)
a = 0
b = 1
t = np.arange(a,b,0.01)
y1 = g(t)
y2 = h(t)


x = np.random.uniform(a,b,n)
yrand = np.random.uniform(0,np.max(y1),n)
yreal1 = g(x)
yreal2 = h(x)
cond1 = np.abs(yrand) <= np.abs(yreal2)
cond2 = np.abs(yrand) >= np.abs(yreal1)
cond3 = cond1 & cond2
puntden = np.sum(cond3)
area2 = np.max(y)*(b-a)*puntden/n
print(area2)

print(integrate(h(z), (z,a,b)) - integrate(g(z), (z,a,b)) )