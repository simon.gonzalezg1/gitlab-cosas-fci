import numpy as np
from sympy import symbols, integrate
import matplotlib.pyplot as plt
# Parametros
Rp = 0.1
Rac = 1
n = 100

rs1,rs2 = np.random.uniform(0,Rac-Rp,(2,n))
th1,th2 = np.random.uniform(0, 2*np.pi, (2,n))

#cond1 = (rs1 == rs2) & (th1 == th2)
dist = np.sqrt(rs1**2 + rs2**2 - 2*rs1*rs2*np.cos(th2-th1))
cond2 = dist<Rp

x1 = rs1[cond2]*np.cos(th1[cond2])
x2 = rs2[cond2]*np.cos(th2[cond2])
y1 = rs1[cond2]*np.sin(th1[cond2])
y2 = rs2[cond2]*np.sin(th2[cond2])

#print(cond2, np.sum(cond2))

fig, ax = plt.subplots()
"""
for i in range(len(x1)):
    circles1 = plt.Circle( (x1[i],y1[i]), Rp, fill = False)
    circles2 = plt.Circle( (x2[i],y2[i]), Rp, fill = False)
    ax.add_patch(circles1)
    ax.add_patch(circles2)
"""

#plt.show()

n = range(8)
prop = np.array([])
for i in n:
    cont = 10**(i)
    rs1,rs2 = np.random.uniform(0,Rac-Rp,(2,cont))
    th1,th2 = np.random.uniform(0, 2*np.pi, (2,cont))
    dist = np.sqrt(rs1**2 + rs2**2 - 2*rs1*rs2*np.cos(th2-th1))
    cond2 = dist<Rp
    prop = np.append(prop, cond2.sum()/cont)

plt.figure()
plt.plot(n, prop)
plt.show()