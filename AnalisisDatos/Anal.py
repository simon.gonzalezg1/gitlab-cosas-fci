import numpy as np
list = np.random.randint(0,10,(2,10))
def promedioarreglos(list):
    prom =  np.array([])
    for i in range(list.shape[0]):
        n = np.sum(list[i,:])
        n = n/list.shape[1]
        prom = np.append(prom, n)
    return prom

def promedioposiciones(list):
    prom =  np.array([])
    for i in range(list.shape[1]):
        n = np.sum(list[:,i])
        n = n/list.shape[0]
        prom = np.append(prom, n)
    return prom

print(list)
print(promedioarreglos(list))
print(promedioposiciones(list))

# O usar np.mean con los axis


